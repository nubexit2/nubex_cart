<?php

namespace Nubex\Cart\Controller\Minicart;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

class Cart extends Action
{
    protected $checkoutSession;
    protected $quoteItem;
    protected $cart;
    protected $quote;
    protected $customerSession;

    public function __construct(
        Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Model\Quote\Item $quoteItem,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Quote\Model\Quote $quote,
        \Magento\Customer\Model\Session $customerSession
    )
    {
        parent::__construct($context);
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
        $this->quoteItem = $quoteItem;
        $this->cart = $cart;
        $this->quote = $quote;
    }

    public function execute()
    {
        /* @var  \Magento\Quote\Model\Quote $quote */
        $quote = $this->checkoutSession->getQuote();
        $quote->setIsActive(false)->save();

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}
